Containers
- [std::cell](#sc)
- [std::cell::RefCell](#rc)

## Containers
<a name=sc></a>
### std::cell
Shareable mutable containers

<a name=rc></a>
### Struct std::cell::RefCell
A mutable memory location with dynamically checked borrow rules
