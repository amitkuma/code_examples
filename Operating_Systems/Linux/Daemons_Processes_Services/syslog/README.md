**syslog**

### syslog (System Logging Protocol)
Helps in transfering information from network devices to a central server(known as syslog server). This logging protocol is a crucial part of network monitoring as it helps you track the overall health of network devices by simplifying log message management.
